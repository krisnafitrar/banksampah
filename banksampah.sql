-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 15 Apr 2020 pada 06.37
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `banksampah`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `desa`
--

CREATE TABLE `desa` (
  `iddesa` int(11) NOT NULL,
  `namadesa` varchar(255) DEFAULT NULL,
  `info_tambahan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `desa`
--

INSERT INTO `desa` (`iddesa`, `namadesa`, `info_tambahan`) VALUES
(1, 'Sumberjo', 'N/A'),
(3, 'Tambakrejo', 'N/A');

-- --------------------------------------------------------

--
-- Struktur dari tabel `dusun`
--

CREATE TABLE `dusun` (
  `iddusun` int(11) NOT NULL,
  `namadusun` varchar(128) DEFAULT NULL,
  `iddesa` int(11) DEFAULT NULL,
  `info_tambahan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `dusun`
--

INSERT INTO `dusun` (`iddusun`, `namadusun`, `iddesa`, `info_tambahan`) VALUES
(1, 'Dusunku', 1, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `golongan_sampah`
--

CREATE TABLE `golongan_sampah` (
  `idgolongan` int(11) NOT NULL,
  `golongan` varchar(128) DEFAULT NULL,
  `info_tambahan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `golongan_sampah`
--

INSERT INTO `golongan_sampah` (`idgolongan`, `golongan`, `info_tambahan`) VALUES
(1, 'Sampah Plastik', 'Polusi plastik adalah akumulasi dari produk plastik yang ada di lingkungan yang berdampak buruk terhadap satwa liar, habitat satwa liar, dan manusia.'),
(2, 'Kardus Bekas', 'Limbah kardus yaitu material yang tak terpakai/tak digunakan dimana material tersebut berasal dari kardus yang telah usang'),
(3, 'Logam', 'Logam (metal) adalah sebuah unsur kimia yang siap membentuk ion (kation) dan memiliki ikatan logam. '),
(4, 'Sampah Kertas', 'Limbah kertas berupa limbah rejek industri kertas yang di daur ulang bisa diolah dan dijadikan sebagai bahan bakar.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `inbox`
--

CREATE TABLE `inbox` (
  `idinbox` int(11) NOT NULL,
  `namapengirim` varchar(255) DEFAULT NULL,
  `emailpengirim` varchar(255) DEFAULT NULL,
  `subjek` varchar(255) DEFAULT NULL,
  `pesan` text DEFAULT NULL,
  `waktukirim` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `inbox`
--

INSERT INTO `inbox` (`idinbox`, `namapengirim`, `emailpengirim`, `subjek`, `pesan`, `waktukirim`) VALUES
(1, 'Krisna', 'krisnafffr@gmail.com', 'Coba', 'Aku mau coba kirim pesan', '2020-04-15 00:20:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis_sampah`
--

CREATE TABLE `jenis_sampah` (
  `idjenis` int(11) NOT NULL,
  `namajenis` varchar(255) DEFAULT NULL,
  `idgolongan` int(11) DEFAULT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `hargasampah` int(11) DEFAULT NULL,
  `info_tambahan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `jenis_sampah`
--

INSERT INTO `jenis_sampah` (`idjenis`, `namajenis`, `idgolongan`, `gambar`, `hargasampah`, `info_tambahan`) VALUES
(1, 'Karpet Plastik', 1, 'plastik_bening.jpg', 1200, NULL),
(2, 'Plastik Bening Campur', 1, 'plastik.jpg', 1500, NULL),
(3, 'Kertas HVS', 4, 'hvs.jpg', 1800, NULL),
(4, 'Kertas Buram', 4, 'buram.jpg', 2000, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `nasabah`
--

CREATE TABLE `nasabah` (
  `idnasabah` int(11) NOT NULL,
  `namanasabah` varchar(255) DEFAULT NULL,
  `jeniskelamin` int(11) DEFAULT NULL,
  `notelp` varchar(16) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `iddesa` int(11) DEFAULT NULL,
  `idrtrw` int(11) DEFAULT NULL,
  `iddusun` int(11) DEFAULT NULL,
  `alamatnasabah` text DEFAULT NULL,
  `foto` varchar(128) DEFAULT NULL,
  `saldonasabah` int(11) DEFAULT NULL,
  `date_created` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `nasabah`
--

INSERT INTO `nasabah` (`idnasabah`, `namanasabah`, `jeniskelamin`, `notelp`, `email`, `iddesa`, `idrtrw`, `iddusun`, `alamatnasabah`, `foto`, `saldonasabah`, `date_created`) VALUES
(1, 'Budi Sudarsono', 1, '08243223', 'budi@gmail.com', 1, 1, 1, 'Desa Sumberjo', 'user.jpg', 0, '2020-04-14 04:09:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `penarikan`
--

CREATE TABLE `penarikan` (
  `kodepenarikan` varchar(128) NOT NULL,
  `idnasabah` int(11) DEFAULT NULL,
  `jumlahpenarikan` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `admin` int(11) DEFAULT NULL,
  `waktupenarikan` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengaturan`
--

CREATE TABLE `pengaturan` (
  `idpengaturan` varchar(128) NOT NULL,
  `namapengaturan` varchar(128) DEFAULT NULL,
  `valuepengaturan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengaturan`
--

INSERT INTO `pengaturan` (`idpengaturan`, `namapengaturan`, `valuepengaturan`) VALUES
('alamat_toko', 'Alamat', 'JL.Diponegoro No.33 Bojonegoro'),
('call_center', 'Call Center', '+62 823 3214 2444'),
('email_center', 'Email Center', 'banksampah@gmail.com'),
('nama_aplikasi', 'Nama Aplikasi', 'Bank Sampah'),
('tentang_aplikasi', 'Tentang', 'Sampah organik adalah sampah yang terdiri dari bahan-bahan penyusun tumbuhan dan hewan yang berasal dari alam atau dihasilkan dari kegiatan pertanian, perikanan, barang rumah tangga atau yang lain. Sampah ini dengan mudah diuraikan dalam proses alami. Banyak sampah rumah tangga yang sebagian besar merupakan bahan organik, misalnya sampah dari dapur, sayuran, kulit buah, dan daun.');

-- --------------------------------------------------------

--
-- Struktur dari tabel `riwayat_saldo`
--

CREATE TABLE `riwayat_saldo` (
  `idriwayat` int(11) NOT NULL,
  `koderiwayat` varchar(128) DEFAULT NULL,
  `idnasabah` int(11) DEFAULT NULL,
  `riwayatsaldo` int(11) DEFAULT NULL,
  `status` varchar(64) DEFAULT NULL,
  `admin` int(11) DEFAULT NULL,
  `insert_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `rtrw`
--

CREATE TABLE `rtrw` (
  `idrtrw` int(11) NOT NULL,
  `rtrw` varchar(128) DEFAULT NULL,
  `iddusun` int(11) DEFAULT NULL,
  `iddesa` int(11) DEFAULT NULL,
  `info_tambahan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `rtrw`
--

INSERT INTO `rtrw` (`idrtrw`, `rtrw`, `iddusun`, `iddesa`, `info_tambahan`) VALUES
(1, '08/02', 1, 1, 'N/A');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `kodetransaksi` varchar(128) NOT NULL,
  `idnasabah` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `admin` int(11) DEFAULT NULL,
  `waktutransaksi` date DEFAULT NULL,
  `insert_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi_detail`
--

CREATE TABLE `transaksi_detail` (
  `iddetail` int(11) NOT NULL,
  `kodetransaksi` varchar(128) DEFAULT NULL,
  `idjenis` int(11) DEFAULT NULL,
  `berat` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `userrole`
--

CREATE TABLE `userrole` (
  `idrole` int(11) NOT NULL,
  `role` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `userrole`
--

INSERT INTO `userrole` (`idrole`, `role`) VALUES
(1, 'Admin'),
(2, 'Nasabah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `iduser` int(11) NOT NULL,
  `idnasabah` int(11) DEFAULT NULL,
  `nama` varchar(128) DEFAULT NULL,
  `username` varchar(128) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `idrole` int(1) DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL,
  `create_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`iduser`, `idnasabah`, `nama`, `username`, `email`, `password`, `idrole`, `is_aktif`, `create_at`) VALUES
(1, NULL, 'Administrator', 'admin', 'adminbanksampah@gmail.com', '$2y$10$hbEtfnIU5y4cgbc15m7jQevyafL9iGJcbVgDsdo2dVhaRzhkQwaeC', 1, 1, '2020-04-14 00:23:59'),
(2, 1, 'User Biasa', 'user', 'user@gmail.com', '$2y$10$HMi6TMMTvYJH.imRUKl5c.YR8pBvLSWz8q9H6Im0g/KS2XcajNl4m', 2, 1, '2020-04-14 09:24:21');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `desa`
--
ALTER TABLE `desa`
  ADD PRIMARY KEY (`iddesa`);

--
-- Indeks untuk tabel `dusun`
--
ALTER TABLE `dusun`
  ADD PRIMARY KEY (`iddusun`),
  ADD KEY `iddesa` (`iddesa`);

--
-- Indeks untuk tabel `golongan_sampah`
--
ALTER TABLE `golongan_sampah`
  ADD PRIMARY KEY (`idgolongan`);

--
-- Indeks untuk tabel `inbox`
--
ALTER TABLE `inbox`
  ADD PRIMARY KEY (`idinbox`);

--
-- Indeks untuk tabel `jenis_sampah`
--
ALTER TABLE `jenis_sampah`
  ADD PRIMARY KEY (`idjenis`),
  ADD KEY `idgolongan` (`idgolongan`);

--
-- Indeks untuk tabel `nasabah`
--
ALTER TABLE `nasabah`
  ADD PRIMARY KEY (`idnasabah`),
  ADD KEY `idrt` (`idrtrw`),
  ADD KEY `iddusun` (`iddusun`),
  ADD KEY `iddesa` (`iddesa`);

--
-- Indeks untuk tabel `penarikan`
--
ALTER TABLE `penarikan`
  ADD PRIMARY KEY (`kodepenarikan`),
  ADD KEY `idnasabah` (`idnasabah`),
  ADD KEY `admin` (`admin`);

--
-- Indeks untuk tabel `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`idpengaturan`),
  ADD UNIQUE KEY `idpengaturan` (`idpengaturan`);

--
-- Indeks untuk tabel `riwayat_saldo`
--
ALTER TABLE `riwayat_saldo`
  ADD PRIMARY KEY (`idriwayat`),
  ADD UNIQUE KEY `koderiwayat` (`koderiwayat`),
  ADD KEY `idnasabah` (`idnasabah`),
  ADD KEY `admin` (`admin`);

--
-- Indeks untuk tabel `rtrw`
--
ALTER TABLE `rtrw`
  ADD PRIMARY KEY (`idrtrw`),
  ADD KEY `iddesa` (`iddesa`),
  ADD KEY `iddusun` (`iddusun`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`kodetransaksi`),
  ADD UNIQUE KEY `kodetransaksi` (`kodetransaksi`);

--
-- Indeks untuk tabel `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  ADD PRIMARY KEY (`iddetail`),
  ADD KEY `kodetransaksi` (`kodetransaksi`),
  ADD KEY `idjenis` (`idjenis`);

--
-- Indeks untuk tabel `userrole`
--
ALTER TABLE `userrole`
  ADD PRIMARY KEY (`idrole`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iduser`),
  ADD KEY `idnasabah` (`idnasabah`),
  ADD KEY `idrole` (`idrole`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `desa`
--
ALTER TABLE `desa`
  MODIFY `iddesa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `dusun`
--
ALTER TABLE `dusun`
  MODIFY `iddusun` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `golongan_sampah`
--
ALTER TABLE `golongan_sampah`
  MODIFY `idgolongan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `inbox`
--
ALTER TABLE `inbox`
  MODIFY `idinbox` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `jenis_sampah`
--
ALTER TABLE `jenis_sampah`
  MODIFY `idjenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `nasabah`
--
ALTER TABLE `nasabah`
  MODIFY `idnasabah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `riwayat_saldo`
--
ALTER TABLE `riwayat_saldo`
  MODIFY `idriwayat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `rtrw`
--
ALTER TABLE `rtrw`
  MODIFY `idrtrw` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  MODIFY `iddetail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `userrole`
--
ALTER TABLE `userrole`
  MODIFY `idrole` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `dusun`
--
ALTER TABLE `dusun`
  ADD CONSTRAINT `dusun_ibfk_1` FOREIGN KEY (`iddesa`) REFERENCES `desa` (`iddesa`);

--
-- Ketidakleluasaan untuk tabel `jenis_sampah`
--
ALTER TABLE `jenis_sampah`
  ADD CONSTRAINT `jenis_sampah_ibfk_1` FOREIGN KEY (`idgolongan`) REFERENCES `golongan_sampah` (`idgolongan`);

--
-- Ketidakleluasaan untuk tabel `nasabah`
--
ALTER TABLE `nasabah`
  ADD CONSTRAINT `nasabah_ibfk_1` FOREIGN KEY (`iddusun`) REFERENCES `dusun` (`iddusun`),
  ADD CONSTRAINT `nasabah_ibfk_2` FOREIGN KEY (`idrtrw`) REFERENCES `rtrw` (`idrtrw`),
  ADD CONSTRAINT `nasabah_ibfk_3` FOREIGN KEY (`iddesa`) REFERENCES `desa` (`iddesa`);

--
-- Ketidakleluasaan untuk tabel `penarikan`
--
ALTER TABLE `penarikan`
  ADD CONSTRAINT `penarikan_ibfk_1` FOREIGN KEY (`idnasabah`) REFERENCES `nasabah` (`idnasabah`),
  ADD CONSTRAINT `penarikan_ibfk_2` FOREIGN KEY (`admin`) REFERENCES `users` (`iduser`);

--
-- Ketidakleluasaan untuk tabel `riwayat_saldo`
--
ALTER TABLE `riwayat_saldo`
  ADD CONSTRAINT `riwayat_saldo_ibfk_1` FOREIGN KEY (`idnasabah`) REFERENCES `nasabah` (`idnasabah`),
  ADD CONSTRAINT `riwayat_saldo_ibfk_2` FOREIGN KEY (`admin`) REFERENCES `users` (`iduser`);

--
-- Ketidakleluasaan untuk tabel `rtrw`
--
ALTER TABLE `rtrw`
  ADD CONSTRAINT `rtrw_ibfk_1` FOREIGN KEY (`iddesa`) REFERENCES `desa` (`iddesa`),
  ADD CONSTRAINT `rtrw_ibfk_2` FOREIGN KEY (`iddusun`) REFERENCES `dusun` (`iddusun`);

--
-- Ketidakleluasaan untuk tabel `transaksi_detail`
--
ALTER TABLE `transaksi_detail`
  ADD CONSTRAINT `transaksi_detail_ibfk_1` FOREIGN KEY (`kodetransaksi`) REFERENCES `transaksi` (`kodetransaksi`),
  ADD CONSTRAINT `transaksi_detail_ibfk_2` FOREIGN KEY (`idjenis`) REFERENCES `jenis_sampah` (`idjenis`);

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`idnasabah`) REFERENCES `nasabah` (`idnasabah`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`idrole`) REFERENCES `userrole` (`idrole`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
