<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-6">
                <?= $this->session->flashdata('message'); ?>
            </div>
        </div>
        <div class="jumbotron">
            <h1 class="display-4">Selamat datang, <?= $user['nama']; ?></h1>
            <p class="lead">Sistem Informasi <?= settingSIM()['nama_aplikasi'] ?></p>
        </div>
        <!-- /.container-fluid -->
    </div>
    <!-- End of Main Content -->
</div>