<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">
        <!-- Page Heading -->
        <h3 class="mb-4 text-dark"><b><?= $title; ?></b></h3>
        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <?php if (validation_errors()) : ?>
                            <div class="alert alert-danger" role="alert">
                                <?= validation_errors(); ?>
                            </div>
                        <?php endif; ?>
                        <?= $this->session->flashdata('message'); ?>
                        <table class="table table-hover" id="tabel-riwayat">
                            <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">Kode Transaksi</th>
                                    <th scope="col">Waktu</th>
                                    <th scope="col">Nasabah</th>
                                    <th scope="col">Grand Total</th>
                                    <th scope="col">Insert At</th>
                                    <th scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $i = 1; ?>
                                <?php foreach ($riwayat as $s) : ?>
                                    <tr>
                                        <td><?= $i++ ?></td>
                                        <td><?= $s['kodetransaksi']; ?></td>
                                        <td><?= $s['waktutransaksi']; ?></td>
                                        <td><?= $s['namanasabah']; ?></td>
                                        <td><?= toRupiah($s['total']); ?></td>
                                        <td><?= $s['insert_at'] ?></td>
                                        <td>
                                            <button type="button" data-type="detail" data-id="<?= rawurlencode($s['kodetransaksi']); ?>" class="btn btn-sm btn-info">Detail</button>
                                            <button type="button" data-type="cancel" data-id="<?= rawurlencode($s['kodetransaksi']); ?>" class="btn btn-sm btn-danger">Batalkan</button>
                                        </td>
                                    </tr>
                                    <?php $i++; ?>
                                <?php endforeach; ?>
                                <?php if (count($riwayat) < 1) : ?>
                                    <tr>
                                        <td colspan="7" align="center" class="text-danger">Transaksi tidak ditemukan</td>
                                    </tr>
                                <?php endif; ?>
                            </tbody>
                        </table>
                        <input type="hidden" name="act" id="act">
                        <input type="hidden" name="key" id="key">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<script>
    $('[data-type=detail]').click(function() {
        var id = $(this).attr('data-id');
        location.href = `<?= base_url('transaksi/detail/') ?>${id}`;
    });

    $(function() {
        $('[data-type="cancel"]').click(function() {
            var id = $(this).attr('data-id');
            bootbox.confirm("Apakah anda ingin membatalkan transaksi?", function(result) {
                if (result) {
                    location.href = '<?= base_url('transaksi/batalkan/') ?>' + id;
                }
            });

        })
    });

    $('#tabel-riwayat').DataTable();
</script>