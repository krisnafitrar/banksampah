  <!-- ##### Hero Area Start ##### -->
  <div class="hero-area">
      <div class="welcome-slides owl-carousel">

          <!-- Single Welcome Slides -->
          <div class="single-welcome-slides bg-img bg-overlay jarallax" style="background-image: url(<?= base_url('assets/img/tabungan.jpg') ?>);">
              <div class="container h-100">
                  <div class="row h-100 align-items-center">
                      <div class="col-12 col-lg-10">
                          <div class="welcome-content">
                              <h2 data-animation="fadeInUp" data-delay="200ms">Penarikan</h2>
                              <p data-animation="fadeInUp" data-delay="400ms">Saldo hanya dapat ditarik jika tidak dalam proses penarikan</p>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

      </div>
  </div>
  <div class="container mt-5 mb-5">
      <div class="d-flex justify-content-center">
          <ul class="nav nav-pills">
              <li class="nav-item">
                  <a class="nav-link <?= $active == 'penarikan' ? 'active' : '' ?>" href="<?= base_url('tabungan/penarikan') ?>">Penarikan</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link <?= $active == 'riwayat' ? 'active' : '' ?>" href="<?= base_url('tabungan/riwayat') ?>">Riwayat</a>
              </li>
          </ul>
      </div>
      <div class="d-flex justify-content-center mt-3">
          <?php if ($active == 'penarikan') : ?>
              <div class="card" style="width: 28rem;">
                  <div class="card-body">
                      <?php if ($penarikan) : ?>
                          <h5 class="card-title">Penarikan</h5>
                          <h6 class="card-subtitle mb-2 text-muted">Bukti Penarikan</h6>
                          <table class="table">
                              <tr>
                                  <td>Kode Penarikan</td>
                                  <td>:</td>
                                  <td><?= $penarikan['kodepenarikan'] ?></td>
                              </tr>
                              <tr>
                                  <td>Jumlah</td>
                                  <td>:</td>
                                  <td><?= toRupiah($penarikan['jumlahpenarikan']) ?></td>
                              </tr>
                              <tr>
                                  <td>Waktu</td>
                                  <td>:</td>
                                  <td><?= $penarikan['waktupenarikan'] ?></td>
                              </tr>
                          </table>
                          <small>Tunjukkan bukti ini ke admin</small>
                      <?php else : ?>
                          <center>
                              <small class="text-danger">Tidak ada penarikan saat ini</small>
                          </center>
                      <?php endif; ?>
                  </div>
              </div>
          <?php elseif ($active == 'riwayat') : ?>
              <div class="col-md-8">
                  <table class="table table-sm">
                      <thead>
                          <tr>
                              <th scope="col">#</th>
                              <th scope="col">Kode Riwayat</th>
                              <th scope="col">Saldo</th>
                              <th scope="col">Waktu</th>
                          </tr>
                      </thead>
                      <tbody>
                          <?php $i = 1; ?>
                          <?php foreach ($riwayatsaldo as $r) : ?>
                              <tr>
                                  <th><?= $i++; ?></th>
                                  <td><?= $r['koderiwayat'] ?></td>
                                  <td><?= $r['status'] == 'plus' ? '+ ' . toRupiah($r['riwayatsaldo']) : '- ' . toRupiah($r['riwayatsaldo']) ?></td>
                                  <td><?= $r['insert_at'] ?></td>
                              </tr>
                          <?php endforeach; ?>
                      </tbody>
                  </table>
              </div>
          <?php endif; ?>
      </div>
  </div>