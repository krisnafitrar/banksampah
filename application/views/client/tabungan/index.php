  <!-- ##### Hero Area Start ##### -->
  <div class="hero-area">
      <div class="welcome-slides owl-carousel">

          <!-- Single Welcome Slides -->
          <div class="single-welcome-slides bg-img bg-overlay jarallax" style="background-image: url(<?= base_url('assets/img/tabungan.jpg') ?>);">
              <div class="container h-100">
                  <div class="row h-100 align-items-center">
                      <div class="col-12 col-lg-10">
                          <div class="welcome-content">
                              <h2 data-animation="fadeInUp" data-delay="200ms">Tabunganku</h2>
                              <p data-animation="fadeInUp" data-delay="400ms">Anda dapat melakukan penarikan saldo di tempat kami</p>
                              <a href="#" class="btn famie-btn mt-4" data-animation="bounceInUp" data-delay="600ms">Kontak Kami</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

      </div>
  </div>
  <!-- ##### Hero Area End ##### -->

  <div class="container mt-5 mb-5">
      <h2 align="center">Tabunganku</h2>
      <div class="d-flex justify-content-center">
          <div class="row">
              <div class="col-md-8">
                  <div class="card mb-3 mt-3" style="max-width: 540px;">
                      <div class="row no-gutters">
                          <div class="col-md-4">
                              <img src="<?= base_url('assets/img/saving.png') ?>" class="card-img" alt="">
                          </div>
                          <div class="col-md-8">
                              <div class="card-body">
                                  <h5 class="card-title">Tabungan Sampah</h5>
                                  <p class="card-text">Dengan menggunakan tabungan ini anda dapat melakukan penarikan saldo di toko kami</p>
                                  <table class="table">
                                      <tr>
                                          <td>Nama</td>
                                          <td>:</td>
                                          <td><?= $nasabah['namanasabah'] ?></td>
                                      </tr>
                                      <tr>
                                          <td>Alamat</td>
                                          <td>:</td>
                                          <td><?= $nasabah['alamatnasabah'] ?></td>
                                      </tr>
                                      <tr>
                                          <td>Desa</td>
                                          <td>:</td>
                                          <td><?= $nasabah['namadesa'] ?></td>
                                      </tr>
                                      <tr>
                                          <td>Dusun</td>
                                          <td>:</td>
                                          <td><?= $nasabah['namadusun'] ?></td>
                                      </tr>
                                      <tr>
                                          <td>RT/RW</td>
                                          <td>:</td>
                                          <td><?= $nasabah['rtrw'] ?></td>
                                      </tr>
                                      <tr>
                                          <td>Saldo</td>
                                          <td>:</td>
                                          <td><?= toRupiah($nasabah['saldonasabah']) ?></td>
                                      </tr>
                                  </table>
                                  <a href="<?= base_url('tabungan/riwayat/' . $nasabah['idnasabah']) ?>" class="btn btn-success btn-block mb-2">Cek Riwayat</a>
                                  <p class="card-text"><small class="text-muted">Member sejak <?= $nasabah['date_created'] ?></small></p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="col-md-4">
                  <div class="card mt-3" style="width: 18rem;">
                      <div class="card-body">
                          <h5 class="card-title">Penarikan Saldo</h5>
                          <?= $this->session->flashdata('message') ?>
                          <form action="<?= base_url('tabungan/tariksaldo') ?>" method="POST">
                              <div class="form-group">
                                  <input type="number" class="form-control" name="nominal" placeholder="Nominal penarikan" required>
                                  <input type="hidden" name="idnasabah" value="<?= $nasabah['idnasabah'] ?>">
                              </div>
                              <div class="form-group">
                                  <button type="submit" class="btn btn-primary btn-block">Tarik Saldo</button>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>