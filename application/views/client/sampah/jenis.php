  <!-- ##### Our Products Area Start ##### -->
  <section class="our-products-area section-padding-100">
      <div class="container">
          <div class="row">
              <div class="col-12">
                  <!-- Section Heading -->
                  <div class="section-heading text-center">
                      <p>Yuk Kenali</p>
                      <h2><span>Jenis</span> Sampah</h2>
                      <img src="img/core-img/decor2.png" alt="">
                  </div>
              </div>
          </div>

          <div class="row">

              <?php foreach ($jenis as $j) : ?>
                  <div class="col-12 col-sm-6 col-lg-3">
                      <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="100ms">
                          <!-- Product Thumbnail -->
                          <div class="product-thumbnail">
                              <img src="<?= base_url('assets/img/jenis/' . $j['gambar']) ?>" alt="">
                              <!-- Product Tags -->
                              <span class="product-tags">Info</span>
                          </div>
                          <!-- Product Description -->
                          <div class="product-desc text-center pt-4">
                              <a href="#" class="product-title"><?= $j['namajenis'] ?></a>
                              <h6 class="price"><?= toRupiah($j['hargasampah']) ?> / Kg</h6>
                          </div>
                      </div>
                  </div>
              <?php endforeach; ?>

          </div>
      </div>
  </section>