  <!-- ##### Services Area Start ##### -->
  <section class="services-area d-flex flex-wrap">
      <!-- Service Thumbnail -->
      <div class="services-thumbnail bg-img jarallax" style="background-image: url('<?= base_url('assets/img/garbage.png') ?>');"></div>

      <!-- Service Content -->
      <div class="services-content section-padding-100-50 px-5">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-12">
                      <!-- Section Heading -->
                      <div class="section-heading">
                          <p>Yuk Pelajari</p>
                          <h2><span>Golongan</span> Sampah</h2>
                          <img src="<?= base_url('assets/img/golongan.jpg') ?>" alt="">
                      </div>
                  </div>
              </div>

              <div class="row">
                  <div class="col-12 mb-50">
                      <p>Dalam Undang-Undang Nomor 18 tahun 2008 tentang pengelolaan mengenai sampah. Definisi sampah adalah sisa-sisa kegiatan setiap hari dari manusia, atau dari proses alam yang terjadi.
                          Bahkan, Indonesia dijuluki sebagai pembuang sampah plastik ke laut terbesar kedua di dunia setelah China. Banyaknya sampah yang dihasilkan oleh warga Indonesia, kini menjadi masalah tersendiri bagi pemerintah. Namun, sejujurnya ini bukan hanya masalah bagi pemerintah saja, tetapi juga tiap warga Indonesia.</p>
                  </div>

                  <!-- Single Service Area -->
                  <?php foreach ($golongan as $gol) : ?>
                      <div class="col-12 col-lg-6">
                          <div class="single-service-area mb-50 wow fadeInUp" data-wow-delay="100ms">
                              <!-- Service Title -->
                              <div class="service-title mb-3 d-flex align-items-center">
                                  <img src="img/core-img/s1.png" alt="">
                                  <h5><?= $gol['golongan'] ?></h5>
                              </div>
                              <p><?= $gol['info_tambahan'] ?></p>
                          </div>
                      </div>
                  <?php endforeach; ?>

              </div>
          </div>
      </div>
  </section>
  <!-- ##### Services Area End ##### -->