  <!-- ##### Contact Information Area Start ##### -->
  <section class="contact-info-area">
      <div class="container">
          <div class="row">
              <div class="col-12">
                  <!-- Section Heading -->
                  <div class="section-heading text-center">
                      <p>INFO Kontak</p>
                      <h2><span>Cara Terbaik</span> Untuk Menghubungi Kami</h2>
                      <img src="img/core-img/decor2.png" alt="">
                  </div>
              </div>
          </div>

          <div class="row">

              <!-- Single Information Area -->
              <div class="col-12 col-md-4">
                  <div class="single-information-area text-center mb-100 wow fadeInUp" data-wow-delay="100ms">
                      <div class="contact-icon">
                          <i class="icon_pin_alt"></i>
                      </div>
                      <h5>Alamat</h5>
                      <h6><?= settingSIM()['alamat_toko'] ?></h6>
                  </div>
              </div>

              <!-- Single Information Area -->
              <div class="col-12 col-md-4">
                  <div class="single-information-area text-center mb-100 wow fadeInUp" data-wow-delay="500ms">
                      <div class="contact-icon">
                          <i class="icon_phone"></i>
                      </div>
                      <h5>No Telp</h5>
                      <h6><?= settingSIM()['call_center'] ?></h6>
                  </div>
              </div>

              <!-- Single Information Area -->
              <div class="col-12 col-md-4">
                  <div class="single-information-area text-center mb-100 wow fadeInUp" data-wow-delay="1000ms">
                      <div class="contact-icon">
                          <i class="icon_mail_alt"></i>
                      </div>
                      <h5>Email</h5>
                      <h6><?= settingSIM()['email_center'] ?></h6>
                  </div>
              </div>

          </div>
          <div class="c-border"></div>
      </div>
  </section>
  <!-- ##### Contact Information Area End ##### -->