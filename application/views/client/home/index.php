  <!-- ##### Hero Area Start ##### -->
  <div class="hero-area">
      <div class="welcome-slides owl-carousel">

          <!-- Single Welcome Slides -->
          <div class="single-welcome-slides bg-img bg-overlay jarallax" style="background-image: url(<?= base_url('assets/client/img/back.jpg') ?>);">
              <div class="container h-100">
                  <div class="row h-100 align-items-center">
                      <div class="col-12 col-lg-10">
                          <div class="welcome-content">
                              <h2 data-animation="fadeInUp" data-delay="200ms">Lindungi bumi ini dari sampah yang merusak</h2>
                              <p data-animation="fadeInUp" data-delay="400ms">Rubah sampah menjadi berkah.</p>
                              <a href="#" class="btn famie-btn mt-4" data-animation="bounceInUp" data-delay="600ms">Kontak Kami</a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>

      </div>
  </div>
  <!-- ##### Hero Area End ##### -->

  <!-- ##### About Us Area Start ##### -->
  <section class="about-us-area">
      <div class="container">
          <div class="row align-items-center">

              <!-- About Us Content -->
              <div class="row">
                  <div class="col-8 col-md-8">
                      <div class="about-us-content mb-100 mt-5">
                          <!-- Section Heading -->
                          <div class="section-heading">
                              <p>Tentang Kami</p>
                              <h2><span>Biarkan</span> Kami Cerita </h2>
                          </div>
                          <p><?= settingSIM()['tentang_aplikasi'] ?></p>
                          <a href="#" class="btn famie-btn mt-30">Baca Selengkapnya</a>
                      </div>
                  </div>
                  <div class="col-md-4 mt-5">
                      <img class="img-thumbnail" src="<?= base_url('assets/client/img/konten/about.jpg') ?>" alt="">
                  </div>
              </div>

          </div>
      </div>
  </section>
  <!-- ##### About Us Area End ##### -->

  <!-- ##### Services Area Start ##### -->
  <section class="services-area d-flex flex-wrap">
      <!-- Service Thumbnail -->
      <div class="services-thumbnail bg-img jarallax" style="background-image: url('<?= base_url('assets/img/garbage.png') ?>');"></div>

      <!-- Service Content -->
      <div class="services-content section-padding-100-50 px-5">
          <div class="container-fluid">
              <div class="row">
                  <div class="col-12">
                      <!-- Section Heading -->
                      <div class="section-heading">
                          <p>Yuk Pelajari</p>
                          <h2><span>Golongan</span> Sampah</h2>
                          <img src="<?= base_url('assets/img/golongan.jpg') ?>" alt="">
                      </div>
                  </div>
              </div>

              <div class="row">
                  <div class="col-12 mb-50">
                      <p>Dalam Undang-Undang Nomor 18 tahun 2008 tentang pengelolaan mengenai sampah. Definisi sampah adalah sisa-sisa kegiatan setiap hari dari manusia, atau dari proses alam yang terjadi.
                          Bahkan, Indonesia dijuluki sebagai pembuang sampah plastik ke laut terbesar kedua di dunia setelah China. Banyaknya sampah yang dihasilkan oleh warga Indonesia, kini menjadi masalah tersendiri bagi pemerintah. Namun, sejujurnya ini bukan hanya masalah bagi pemerintah saja, tetapi juga tiap warga Indonesia.</p>
                  </div>

                  <!-- Single Service Area -->
                  <?php foreach ($golongan as $gol) : ?>
                      <div class="col-12 col-lg-6">
                          <div class="single-service-area mb-50 wow fadeInUp" data-wow-delay="100ms">
                              <!-- Service Title -->
                              <div class="service-title mb-3 d-flex align-items-center">
                                  <img src="img/core-img/s1.png" alt="">
                                  <h5><?= $gol['golongan'] ?></h5>
                              </div>
                              <p><?= $gol['info_tambahan'] ?></p>
                          </div>
                      </div>
                  <?php endforeach; ?>

              </div>
          </div>
      </div>
  </section>
  <!-- ##### Services Area End ##### -->

  <!-- ##### Our Products Area Start ##### -->
  <section class="our-products-area section-padding-100">
      <div class="container">
          <div class="row">
              <div class="col-12">
                  <!-- Section Heading -->
                  <div class="section-heading text-center">
                      <p>Yuk Kenali</p>
                      <h2><span>Jenis</span> Sampah</h2>
                      <img src="img/core-img/decor2.png" alt="">
                  </div>
              </div>
          </div>

          <div class="row">

              <?php foreach ($jenis as $j) : ?>
                  <div class="col-12 col-sm-6 col-lg-3">
                      <div class="single-product-area mb-50 wow fadeInUp" data-wow-delay="100ms">
                          <!-- Product Thumbnail -->
                          <div class="product-thumbnail">
                              <img src="<?= base_url('assets/img/jenis/' . $j['gambar']) ?>" alt="">
                              <!-- Product Tags -->
                              <span class="product-tags">Info</span>
                          </div>
                          <!-- Product Description -->
                          <div class="product-desc text-center pt-4">
                              <a href="#" class="product-title"><?= $j['namajenis'] ?></a>
                              <h6 class="price"><?= toRupiah($j['hargasampah']) ?> / Kg</h6>
                          </div>
                      </div>
                  </div>
              <?php endforeach; ?>

          </div>

          <div class="row">
              <div class="col-12">
                  <div class="gotoshop-btn text-center wow fadeInUp" data-wow-delay="900ms">
                      <a href="<?= base_url('sampah') ?>" class="btn famie-btn">Lihat Selengkapnya</a>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!-- ##### Our Products Area End ##### -->

  <!-- ##### Testimonial Area End ##### -->

  <!-- ##### Contact Area Start ##### -->
  <section class="contact-area section-padding-100-0">
      <div class="container">
          <div class="row justify-content-between">

              <!-- Contact Content -->
              <div class="col-12 col-lg-5">
                  <div class="contact-content mb-100">
                      <!-- Section Heading -->
                      <div class="section-heading">
                          <p>Kontak Sekarang</p>
                          <h2><span>Tinggalkan Pesan</span> Jika Dibutuhkan</h2>
                          <?= $this->session->flashdata('message') ?>
                          <img src="img/core-img/decor.png" alt="">
                      </div>
                      <!-- Contact Form Area -->
                      <div class="contact-form-area">
                          <form action="<?= base_url('beranda') ?>" method="post">
                              <div class="row">
                                  <div class="col-lg-6">
                                      <input type="text" class="form-control" name="nama" placeholder="Nama Anda" required>
                                  </div>
                                  <div class="col-lg-6">
                                      <input type="email" class="form-control" name="email" placeholder="Email Anda" required>
                                  </div>
                                  <div class="col-12">
                                      <input type="text" class="form-control" name="subjek" placeholder="Subjek" required>
                                  </div>
                                  <div class="col-12">
                                      <textarea name="pesan" class="form-control" cols="30" rows="10" placeholder="Tinggalkan Pesan Disini" required></textarea>
                                  </div>
                                  <div class="col-12">
                                      <button type="submit" class="btn famie-btn">Kirim Pesan</button>
                                  </div>
                              </div>
                          </form>
                      </div>
                  </div>
              </div>

              <!-- Contact Maps -->
              <div class="col-lg-6">
                  <div class="contact-maps mb-100">
                      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d28313.28917392649!2d-88.2740948914384!3d41.76219337461615!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880efa1199df6109%3A0x8a1293b2ae8e0497!2sE+New+York+St%2C+Aurora%2C+IL%2C+USA!5e0!3m2!1sen!2sbd!4v1542893000723" allowfullscreen></iframe>
                  </div>
              </div>
          </div>
      </div>
  </section>
  <!-- ##### Contact Area End ##### -->