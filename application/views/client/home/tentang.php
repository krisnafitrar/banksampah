  <!-- ##### About Us Area Start ##### -->
  <section class="about-us-area">
      <div class="container">
          <div class="row align-items-center">

              <!-- About Us Content -->
              <div class="row">
                  <div class="col-8 col-md-8">
                      <div class="about-us-content mb-100 mt-5">
                          <!-- Section Heading -->
                          <div class="section-heading">
                              <p>Tentang Kami</p>
                              <h2><span>Biarkan</span> Kami Cerita </h2>
                          </div>
                          <p><?= settingSIM()['tentang_aplikasi'] ?></p>
                      </div>
                  </div>
                  <div class="col-md-4 mt-5">
                      <img class="img-thumbnail" src="<?= base_url('assets/client/img/konten/about.jpg') ?>" alt="">
                  </div>
              </div>

          </div>
      </div>
  </section>