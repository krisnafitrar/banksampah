<div class="container">
    <div class="row">
        <div class="col-md-2">
        </div>
        <div class="col-md-8">
            <form method="POST" id="form_data">
                <?php foreach ($a_kolom as $col) { ?>
                    <div class="form-group">
                        <label for="<?= $col['kolom'] ?>"><?= $col['label'] ?></label>
                        <input type="text" class="form-control" name="<?= $col['kolom'] ?>" id="<?= $col['kolom'] ?>" value="<?= !empty($row) ? $row[$col['kolom']] : '' ?>" placeholder="Masukkan <?= $col['label'] ?>" <?= $col['required'] == true ? 'required' : '' ?>>
                    </div>
                <?php } ?>
                <button type="button" data-type="save" class="btn btn-primary py-3 px-4">Simpan</button>
                <input type="hidden" name="act" id="act">
                <input type="hidden" name="key" id="key">
            </form>
        </div>
        <div class="col-md-2">
        </div>
    </div>
</div>
<script>
    $(function() {
        $('[data-type="save"]').click(function() {
            bootbox.confirm("Apakah anda yakin akan menyimpan data ini?", function(result) {
                if (result) {
                    $('#form_data #act').val('save');
                    $('#form_data #key').val('<?= !empty($row) ? $row['idalamat'] : '' ?>');
                    $('#form_data').submit();
                }
            });
        })
    })
</script>