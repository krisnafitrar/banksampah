<div class="container">
    <a href="<?= site_url('profil/alamat/add') ?>" data-type="add" class="btn btn-primary py-3 px-4">Tambah Alamat</a>
    <hr>
    <?= $this->session->flashdata('message'); ?>

    <div class="row">
        <div class="col-md-12 ftco-animate">
            <table class="table">
                <thead class="thead-primary">
                    <tr class="text-center">
                        <?php foreach ($a_kolom as $col) { ?>
                            <th><?= $col['label'] ?></th>
                        <?php } ?>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($alamat > 0) { ?>
                        <?php
                        $no = 0;
                        foreach ($alamat as $row) { ?>
                            <tr>
                                <?php foreach ($a_kolom as $col) { ?>
                                    <?php if ($col['kolom'] == 'no') { ?>
                                        <td width="10px"><?= ++$no ?></td>
                                    <?php } else { ?>
                                        <td><?= $row[$col['kolom']] ?></td>
                                    <?php } ?>
                                <?php } ?>
                                <td width="90px" align="center">
                                    <a href="<?= site_url('profil/alamat/detail/' . $row['idalamat']) ?>" class="btn btn-warning btn-sm" data-toggle="tooltip" title="Detail"><i class="fa fa-eye"></i></a>
                                    <button type="button" data-type="delete" data-id="<?= $row['idalamat'] ?>" class="btn btn-danger btn-sm" data-toggle="tooltip" title="Hapus"><i class="fa fa-trash"></i></button>
                                </td>
                            </tr>
                        <?php } ?>
                    <?php } else { ?>
                        <tr>
                            <td colspan="3" align="center">Tidak ada alamat</td>
                        </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>