<div class="container">
    <div class="card mb-3 mx-auto" style="max-width: 540px;">
        <div class="row no-gutters">
            <div class="col-md-4">
                <img src="<?= base_url('assets/img/default.png') ?>" class="card-img" alt="...">
            </div>
            <div class="col-md-8">
                <div class="card-body">
                    <h5 class="card-title"><?= $users['nama'] ?></h5>
                    <p class="card-text"><?= $users['username'] ?></p>
                    <p class="card-text"><?= $users['email'] ?></p>
                    <p class="card-text"><small class="text-muted">Dibuat pada <?= date('d-M-Y H:i', $users['create_at']) ?></small></p>
                </div>
            </div>
        </div>
    </div>
</div>