<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <!-- Title -->
    <title><?= $title; ?> - <?= settingSIM()['nama_aplikasi'] ?></title>
    <!-- Favicon -->
    <link rel="icon" href="<?= base_url('assets/client/img/core-img/favicon.ico') ?>">
    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="<?= base_url('assets/client/style.css') ?>">


    <link rel="stylesheet" href="<?= base_url('assets/v2/'); ?>vendor/sweetalert2/dist/sweetalert2.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/v2/'); ?>vendor/datatables110/datatables.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/v2/'); ?>vendor/select2/dist/css/select2.min.css">
    <link rel="stylesheet" href="<?= base_url('assets/v2/'); ?>vendor/datepicker/datepicker3.css">
    <!-- <link rel="stylesheet" href="<?= base_url('assets/v2/'); ?>vendor/jquery-ui/jquery-ui.min.css"> -->
    <script src="<?= base_url('assets/v2/'); ?>vendor/jquery/jquery.min.js"></script>
    <!-- <script src="<?= base_url('assets/v2/'); ?>vendor/jquery-ui/jquery-ui.min.js"></script> -->
    <script src="<?= base_url('assets/v2/'); ?>js/jquery.ajax.js"></script>
    <script src="<?= base_url('assets/v2/'); ?>vendor/select2/dist/js/select2.full.min.js"></script>
    <script src="<?= base_url('assets/v2/'); ?>vendor/sweetalert2/dist/sweetalert2.min.js"></script>
    <script src="<?= base_url('assets/v2/'); ?>vendor/datatables110/datatables.min.js"></script>
    <script src="<?= base_url('assets/v2/'); ?>vendor/datepicker/bootstrap-datepicker.js"></script>
    <script src="<?php echo base_url('assets/v2/') ?>vendor/tinymce/tinymce.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>
</head>

<body>
    <!-- Preloader -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <div class="spinner"></div>
    </div>