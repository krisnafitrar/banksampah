  <!-- ##### Header Area Start ##### -->
  <header class="header-area">
      <!-- Top Header Area -->
      <div class="top-header-area">
          <div class="container">
              <div class="row">
                  <div class="col-12">
                      <div class="top-header-content d-flex align-items-center justify-content-between">
                          <!-- Top Header Content -->
                          <div class="top-header-meta">
                              <p>Selamat datang di <span><?= settingSIM()['nama_aplikasi'] ?></span>, semoga anda tetap menjaga kebersihan ya</p>
                          </div>
                          <!-- Top Header Content -->
                          <div class="top-header-meta text-right">
                              <a href="#" data-toggle="tooltip" data-placement="bottom" title="<?= settingSIM()['email_center'] ?>"><i class="fa fa-envelope-o" aria-hidden="true"></i> <span>Email: <?= settingSIM()['email_center'] ?></span></a>
                              <a href="#" data-toggle="tooltip" data-placement="bottom" title="<?= settingSIM()['call_center'] ?>"><i class="fa fa-phone" aria-hidden="true"></i> <span>Call Center: <?= settingSIM()['call_center'] ?></span></a>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>

      <!-- Navbar Area -->
      <div class="famie-main-menu">
          <div class="classy-nav-container breakpoint-off">
              <div class="container">
                  <!-- Menu -->
                  <nav class="classy-navbar justify-content-between" id="famieNav">
                      <!-- Nav Brand -->
                      <a href="index.html" class="nav-brand">
                          <h5><?= settingSIM()['nama_aplikasi'] ?></h5>
                      </a>
                      <!-- Navbar Toggler -->
                      <div class="classy-navbar-toggler">
                          <span class="navbarToggler"><span></span><span></span><span></span></span>
                      </div>
                      <!-- Menu -->
                      <div class="classy-menu">
                          <!-- Close Button -->
                          <div class="classycloseIcon">
                              <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                          </div>
                          <!-- Navbar Start -->
                          <div class="classynav">
                              <ul>
                                  <li class="<?= $active == 'beranda' ? 'active' : '' ?>"><a href="<?= base_url() ?>">Beranda</a></li>
                                  <li><a href="#">Sampah</a>
                                      <ul class="dropdown">
                                          <li class="<?= $active == 'golongan' ? 'active' : '' ?>"><a href="<?= base_url('sampah/golongan') ?>">Golongan Sampah</a></li>
                                          <li class="<?= $active == 'jenis' ? 'active' : '' ?>"><a href="<?= base_url('sampah/index') ?>">Jenis Sampah</a></li>
                                      </ul>
                                  </li>
                                  <li class="<?= $active == 'tentang' ? 'active' : '' ?>"><a href="<?= base_url('beranda/tentang') ?>">Tentang</a></li>
                                  <li class="<?= $active == 'kontak' ? 'active' : '' ?>"><a href="<?= base_url('beranda/kontak') ?>">Kontak</a></li>
                                  <?php if (!empty($this->session->userdata('username'))) : ?>
                                      <li><a href="#"><?= $this->session->userdata('username') ?></a>
                                          <ul class="dropdown">
                                              <li><a href="<?= base_url('beranda/profil') ?>">Edit Profil</a></li>
                                              <li><a href="<?= base_url('tabungan/penarikan') ?>">Penarikan</a></li>
                                              <li><a href="<?= base_url('auth/logout') ?>">Keluar</a></li>
                                          </ul>
                                      </li>
                                  <?php else : ?>
                                      <li><a href="<?= base_url('auth') ?>">Masuk</a></li>
                                  <?php endif; ?>
                              </ul>
                              <!-- Search Icon -->
                              <div id="searchIcon">
                                  <i class="icon_search" aria-hidden="true"></i>
                              </div>
                              <!-- Cart Icon -->
                              <div id="cartIcon">
                                  <?php if (!empty($this->session->userdata('username'))) : ?>
                                      <a href="<?= base_url('tabungan') ?>">
                                          <i class="icon_wallet_alt" aria-hidden="true"></i>
                                      </a>
                                  <?php else : ?>
                                      <a href="<?= base_url('auth') ?>">
                                          <i class="icon_wallet_alt" aria-hidden="true"></i>
                                      </a>
                                  <?php endif; ?>
                              </div>
                          </div>
                          <!-- Navbar End -->
                      </div>
                  </nav>

                  <!-- Search Form -->
                  <div class="search-form">
                      <form action="#" method="get">
                          <input type="search" name="search" id="search" placeholder="Type keywords &amp; press enter...">
                          <button type="submit" class="d-none"></button>
                      </form>
                      <!-- Close Icon -->
                      <div class="closeIcon"><i class="fa fa-times" aria-hidden="true"></i></div>
                  </div>
              </div>
          </div>
      </div>
  </header>
  <!-- ##### Header Area End ##### -->