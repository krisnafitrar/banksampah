<!-- ##### Footer Area Start ##### -->
<footer class="footer-area">
    <!-- Main Footer Area -->
    <div class="main-footer bg-img bg-overlay section-padding-80-0" style="background-image: url(img/bg-img/3.jpg);">
        <div class="container">
            <div class="row">

                <!-- Single Footer Widget Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="footer-widget mb-80">
                        <a href="#" class="foo-logo d-block mb-30">
                            <h5 class="text-white"><b><?= settingSIM()['nama_aplikasi'] ?></b></h5>
                        </a>
                        <p>Lorem ipsum dolor sit amet, consecte stare adipiscing elit. In act honcus risus atiner Pellentesque risus.</p>
                        <div class="contact-info">
                            <p><i class="fa fa-map-pin" aria-hidden="true"></i><span><?= settingSIM()['alamat_toko'] ?></span></p>
                            <p><i class="fa fa-envelope" aria-hidden="true"></i><span><?= settingSIM()['email_center'] ?></span></p>
                            <p><i class="fa fa-phone" aria-hidden="true"></i><span><?= settingSIM()['call_center'] ?></span></p>
                        </div>
                    </div>
                </div>

                <!-- Single Footer Widget Area -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="footer-widget mb-80">
                        <h5 class="widget-title">Hubungi Lewat</h5>
                        <!-- Footer Social Info -->
                        <div class="footer-social-info">
                            <a href="#">
                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                <span>Facebook</span>
                            </a>
                            <a href="#">
                                <i class="fa fa-twitter" aria-hidden="true"></i>
                                <span>Twitter</span>
                            </a>
                            <a href="#">
                                <i class="fa fa-pinterest" aria-hidden="true"></i>
                                <span>Pinterest</span>
                            </a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Copywrite Area  -->
    <div class="copywrite-area">
        <div class="container">
            <div class="copywrite-text">
                <div class="row align-items-center">
                    <div class="col-md-6">
                        <p>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>
                                document.write(new Date().getFullYear());
                            </script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="#" target="_blank">Cicik Project</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- ##### Footer Area End ##### -->

<!-- ##### All Javascript Files ##### -->

<!-- Bootstrap core JavaScript-->
<script src="<?= base_url('assets/v2/'); ?>vendor/jquery/jquery.min.js"></script>
<script src="<?= base_url('assets/v2/'); ?>vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?= base_url('assets/v2/'); ?>vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- jquery 2.2.4  -->
<script src="<?= base_url('assets/client/js/jquery.min.js') ?>"></script>
<!-- Popper js -->
<script src="<?= base_url('assets/client/js/popper.min.js') ?>"></script>
<!-- Bootstrap js -->
<script src="<?= base_url('assets/client/js/bootstrap.min.js') ?>"></script>
<!-- Owl Carousel js -->
<script src="<?= base_url('assets/client/js/owl.carousel.min.js') ?>"></script>
<!-- Classynav -->
<script src="<?= base_url('assets/client/js/classynav.js') ?>"></script>
<!-- Wow js -->
<script src="<?= base_url('assets/client/js/wow.min.js') ?>"></script>
<!-- Sticky js -->
<script src="<?= base_url('assets/client/js/jquery.sticky.js') ?>"></script>
<!-- Magnific Popup js -->
<script src="<?= base_url('assets/client/js/jquery.magnific-popup.min.js') ?>"></script>
<!-- Scrollup js -->
<script src="<?= base_url('assets/client/js/jquery.scrollup.min.js') ?>"></script>
<!-- Jarallax js -->
<script src="<?= base_url('assets/client/js/jarallax.min.js') ?>"></script>
<!-- Jarallax Video js -->
<script src="<?= base_url('assets/client/js/jarallax-video.min.js') ?>"></script>
<!-- Active js -->
<script src="<?= base_url('assets/client/js/active.js') ?>"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.4.0/bootbox.min.js"></script>
</body>

</html>