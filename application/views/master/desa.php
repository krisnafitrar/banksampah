    <!-- Main Content -->
    <div id="content">
        <!-- Begin Page Content -->
        <div class="container-fluid">

            <!-- Page Heading -->
            <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>

            <div class="col-md-10">
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <button type="button" data-type="tambah" class="btn btn-primary mb-3">Tambah <?= $title ?></button>

                        <div class="row">
                            <div class="col-md-12">
                                <?= $this->session->flashdata('message'); ?>
                                <form action="<?= site_url('master/desa') ?>" method="post" id="form-list">
                                    <table class="table table-hover" id="datatable">
                                        <thead>
                                            <tr>
                                                <th scope="col" width="10%">No</th>
                                                <th scope="col">Nama Desa</th>
                                                <th scope="col">Info Tambahan</th>
                                                <th scope="col">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php foreach ($desa as $r) : ?>
                                                <tr>
                                                    <th scope="row"><?= $i; ?></th>
                                                    <td><?= $r['namadesa']; ?></td>
                                                    <td><?= $r['info_tambahan']; ?></td>
                                                    <td>
                                                        <button type="button" data-type="edit" class="btn btn-sm btn-info" data-id="<?= $r['iddesa'] ?>">Edit</button>
                                                        <button type="button" data-type="delete" class="btn btn-sm btn-danger" data-id="<?= $r['iddesa'] ?>">Delete</a>
                                                    </td>
                                                </tr>
                                                <?php $i++; ?>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>

                                    <input type="hidden" name="act" id="act">
                                    <input type="hidden" name="key" id="key">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
        <!-- /.container-fluid -->

    </div>
    <!-- End of Main Content -->

    <!-- Modal -->
    <div class="modal fade" id="newMenuModal" tabindex="-1" role="dialog" aria-labelledby="newMenuModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="newMenuModalLabel">Add New Role</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="<?= base_url('master/role') ?>" method="post" id="modal_post">
                    <div class="modal-body">
                        <div class="form-group">
                            <input type="text" class="form-control" id="namarole" name="namarole" placeholder="Nama Role" required>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" data-type="simpan" class="btn btn-success">Simpan</button>
                    </div>

                    <input type="hidden" name="act" id="act">
                    <input type="hidden" name="key" id="key">
                </form>
            </div>
        </div>
    </div>
    <script>
        $('[data-type=simpan]').click(function() {
            var act = $('#modal_post #act').val();
            var key = $('#modal_post #key').val();
            if (act == "") {
                $('#modal_post #act').val('simpan');
            }
            $('#modal_post').submit();
        });


        $('[data-type=delete]').click(function() {
            var id = $(this).attr('data-id');
            Swal.fire({
                // title: 'Are you sure?',
                text: "Anda yakin akan menghapus role ini?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: 'red',
                cancelButtonColor: 'grey',
                confirmButtonText: 'Hapus',
                cancelButtonText: 'Batal'
            }).then((result) => {
                if (result.value) {
                    $('#form-list #key').val(id);
                    $('#form-list').attr('action', "<?= site_url('master/deleteRole') ?>");
                    $('#form-list').submit();
                }
            })
        });

        $('[data-type=tambah]').click(function() {
            var modal = $('#newMenuModal');
            $('#modal_post')[0].reset();
            modal.find('#newMenuModalLabel').html('Tambah <?= $title ?>');
            // modal.find('#nip').attr('disabled', false);
            modal.modal();
        });

        $('[data-type=edit]').click(function() {
            var id = $(this).attr('data-id');
            Swal.showLoading();
            xhrfGetData("<?= site_url('master/getRole/') ?>" + id, function(data) {
                var modal = $('#newMenuModal');
                modal.find('#newMenuModalLabel').html('Ubah <?= $title ?>');
                modal.find('#namarole').val(data.role);
                modal.find('#act').val('edit');
                modal.find('#key').val(data.id);
                Swal.close();
                modal.modal();
            });
        });
        $('#datatable').DataTable();
    </script>