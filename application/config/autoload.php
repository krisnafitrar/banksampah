<?php
defined('BASEPATH') or exit('No direct script access allowed');

$autoload['packages'] = array();
$autoload['libraries'] = array('database', 'session', 'table', 'form_validation', 'template', 'exceptions', 'breadcrumb');

$autoload['drivers'] = array();

$autoload['helper'] = array('url', 'download', 'form', 'file', 'banksampah');

$autoload['config'] = array();

$autoload['language'] = array();

$autoload['model'] = array();
