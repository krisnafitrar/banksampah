<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_golongansampah extends MY_Model
{
    protected $table = 'golongan_sampah';
    protected $schema = '';
    public $key = 'idgolongan';
    public $value = 'golongan';

    function __construct()
    {
        parent::__construct();
    }
}
