<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_jenissampah extends MY_Model
{
    protected $table = 'jenis_sampah';
    protected $schema = '';
    public $key = 'idjenis';
    public $value = 'namajenis';

    function __construct()
    {
        parent::__construct();
    }
}
