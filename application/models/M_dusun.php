<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_dusun extends MY_Model
{
    protected $table = 'dusun';
    protected $schema = '';
    public $key = 'iddusun';
    public $value = 'namadusun';

    function __construct()
    {
        parent::__construct();
    }
}
