<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_setting extends MY_Model
{
    protected $table = 'pengaturan';
    protected $schema = '';
    public $key = 'idpengaturan';
    public $value = 'valuepengaturan';
}
