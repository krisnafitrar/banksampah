<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_penarikan extends MY_Model
{
    protected $table = 'penarikan';
    protected $schema = '';
    public $key = 'kodepenarikan';
    public $value = 'jumlahpenarikan';

    function __construct()
    {
        parent::__construct();
    }

    public function riwayat($id)
    {
        $query = "SELECT * FROM riwayat_saldo p WHERE p.idnasabah=$id";
        return $this->db->query($query);
    }
}
