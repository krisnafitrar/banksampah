<?php

class M_nasabah extends MY_Model
{
    protected $table = 'nasabah';
    protected $schema = '';
    public $key = 'idnasabah';
    public $value = 'namanasabah';

    function __construct()
    {
        parent::__construct();
    }

    public function getReff($id)
    {
        $query = "SELECT * FROM $this->table JOIN desa USING(iddesa) JOIN dusun USING(iddusun) JOIN rtrw USING(idrtrw) WHERE idnasabah=$id";

        return $this->db->query($query);
    }
}
