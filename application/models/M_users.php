<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_users extends MY_Model
{
    protected $table = 'users';
    protected $schema = '';
    public $key = 'iduser';
    public $value = 'nama';

    function __construct()
    {
        parent::__construct();
    }

    public function getUsers($limit, $start, $keyword = null)
    {
        if ($keyword) {
            $this->db->like('username', $keyword);
        }
        return $this->db->get('user', $limit, $start)->result_array();
    }

    public function getByUsername($username)
    {
        $query = "SELECT * FROM $this->table LEFT JOIN nasabah USING(idnasabah) WHERE username='$username'";

        return $this->db->query($query);
    }
}
