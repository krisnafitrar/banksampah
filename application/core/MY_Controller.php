<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public $class;
    public $title;
    public $menu;
    public $icon;
    public $user;
    public $parent;
    public $a_kolom;
    public $pager = false;
    public $trans = false;
    public $is_crud = true;

    function __construct()
    {
        parent::__construct();
        error_reporting(E_ALL && ~E_NOTICE && ~E_STRICT && ~E_WARNING && ~E_DEPRECATED);

        $this->setController();
    }

    public function setController()
    {
        $this->class = $this->router->fetch_class();

        $filemodel = __DIR__ . '/../models/M_' . strtolower($this->class) . '.php';
        if (file_exists($filemodel)) {
            $this->load->model('M_' . strtolower($this->class), 'model');
        }
        if (!empty($this->session->userdata('username'))) {
            $this->load->model('M_users', 'M_user');
            $this->user = $this->M_user->getBy(['username' => $this->session->userdata('username')])->row_array();
        }
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url('User'));
        $this->breadcrumb->append_crumb($this->title, site_url());

        $data['title'] = $this->title;
        $data['menu'] = $this->menu;
        $data['user'] = $this->user;
        $data['parent'] = $this->parent;
        $data['primary'] = $this->model->getKey();

        if ($this->pager) {
            //set pagination
            $limit = 10;
            $page = 1;
            $total_row = $this->model->getAllRows();
            setPagination($limit, $total_row, $this->parent . '/' . $this->menu . '/index');

            $data['a_kolom'] = $this->a_kolom;
            $data['is_crud'] = $this->is_crud;
            $data['a_data'] = $this->model->getPagerData($this->uri->segment(4), $limit);
        }

        if ($_POST['act'] == 'delete') {
            $ok = $this->model->delete($_POST['key']);
            $message = 'menghapus data';
            ($ok && $ok) ? setMessage('Berhasil ' . $message, 'success') : setMessage('Gagal ' . $message, 'danger');
            redirect($this->parent . '/' . $this->menu);
        }

        $this->template->load('template', strtolower($this->class) . '/index', $data);
    }

    public function create()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url('home'));
        $this->breadcrumb->append_crumb($this->title, site_url($this->parent . '/' . $this->menu));
        $this->breadcrumb->append_crumb('Tambah', site_url());

        $data['title'] = $this->title;
        $data['menu'] = $this->menu;
        $data['parent'] = $this->parent;
        $data['user'] = $this->user;
        $data['a_kolom'] = $this->a_kolom;

        if (!empty($_POST) || !empty($_FILES)) {
            $record = [];
            foreach ($this->a_kolom as $col) {
                if (isset($_POST[$col['kolom']]) || $col['type'] == 'F' || $col['is_null']) {
                    if ($_FILES[$col['kolom']]['name'] != "") {
                        $record[$col['kolom']] = $_FILES[$col['kolom']];
                        $config['upload_path'] = $col['path'];
                        $config['allowed_types'] = $col['file_type'];
                        $config['max_size'] = empty($col['size']) ? 2048 : $col['size'];
                        $this->load->library('upload', $config);

                        if ($this->upload->do_upload($col['kolom'])) {
                            $file = $this->upload->data('file_name');
                        } else {
                            setMessage($this->upload->display_errors(), 'danger');
                            redirect($this->parent . '/' . $this->menu);
                        }
                    }
                    $record[$col['kolom']] = $_FILES[$col['kolom']] ? $file : ($col['type'] != 'P' ? $_POST[$col['kolom']] : password_hash($_POST[$col['kolom']], PASSWORD_DEFAULT));
                } else {
                    setMessage('Mohon lengkapi data', 'danger');
                    redirect($this->parent . '/' . $this->menu);
                }
            }
            $ok = $this->model->insert($record);
            $message = 'menambahkan data baru';
            ($ok && $ok) ? setMessage('Berhasil ' . $message, 'success') : setMessage('Gagal ' . $message, 'danger');
            redirect($this->parent . '/' . $this->menu);
        }

        $this->template->load('template', strtolower($this->class) . '/create', $data);
    }

    public function detail($id)
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> ' . 'Beranda', site_url('home'));
        $this->breadcrumb->append_crumb($this->title, site_url($this->parent . '/' . $this->menu));
        $this->breadcrumb->append_crumb('Detail', site_url());

        $data['title'] = $this->title;
        $data['menu'] = $this->menu;
        $data['parent'] = $this->parent;
        $data['user'] = $this->user;

        $data['a_kolom'] = $this->a_kolom;
        $data['row'] = $this->model->getBy([$this->model->getKey() => $id])->row_array();

        if (!empty($_POST) || !empty($_FILES)) {
            $record = [];
            foreach ($this->a_kolom as $col) {
                if (isset($_POST[$col['kolom']]) || $col['type'] == 'F' || $col['is_null']) {
                    $file = $data['row'][$col['kolom']];
                    if ($_FILES[$col['kolom']]['name'] != "") {
                        $record[$col['kolom']] = $_FILES[$col['kolom']];
                        $config['upload_path'] = $col['path'];
                        $config['allowed_types'] = $col['file_type'];
                        $config['max_size'] = empty($col['size']) ? 2048 : $col['size'];
                        $this->load->library('upload', $config);

                        if ($this->upload->do_upload($col['kolom'])) {
                            $old_image = $data['row'][$col['kolom']];
                            unlink(FCPATH . $col['path'] . $old_image);
                            $file = $this->upload->data('file_name');
                        } else {
                            setMessage($this->upload->display_errors(), 'danger');
                            redirect($this->parent . '/' . $this->menu);
                        }
                    }
                    $record[$col['kolom']] = $_FILES[$col['kolom']] ? $file : ($col['type'] != 'P' ? $_POST[$col['kolom']] : password_hash($_POST[$col['kolom']], PASSWORD_DEFAULT));
                } else {
                    setMessage('Mohon lengkapi data', 'danger');
                    redirect($this->parent . '/' . $this->menu);
                }
            }
            $ok = $this->model->update($record, $id);
            $message = 'mengubah data';
            ($ok && $ok) ? setMessage('Berhasil ' . $message, 'success') : setMessage('Gagal ' . $message, 'danger');

            redirect($this->parent . '/' . $this->menu);
        }

        $this->template->load('template', strtolower($this->class) . '/detail', $data);
    }

    public function defaultView($data, $view)
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-' . $this->icon . '"></i> ' . $this->title, site_url());
        $this->breadcrumb->append_crumb('Dashboard', site_url('home'));

        $data['title'] = $this->title;
        $data['menu'] = $this->menu;
        $data['user'] = $this->user;

        $this->template->load('template', strtolower($this->class) . '/index', $data);
    }
}
