<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Beranda extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_users', 'users');
    }

    public function index()
    {
        $data['title'] = 'Beranda';
        $this->db->limit(4);
        $data['golongan'] = $this->db->get('golongan_sampah')->result_array();
        $this->db->limit(4);
        $data['jenis'] = $this->db->get('jenis_sampah')->result_array();
        $data['active'] = 'beranda';
        $this->template->load('client/templatefront', 'client/home/index', $data);

        if ($_POST) {
            $nama = $this->input->post('nama');
            $email = $this->input->post('email');
            $subjek = $this->input->post('subjek');
            $pesan = $this->input->post('pesan');

            $data = [
                'namapengirim' => $nama,
                'emailpengirim' => $email,
                'subjek' => $subjek,
                'pesan' => $pesan
            ];

            $ok = $this->db->insert('inbox', $data);

            $ok ? setMessage('Berhasil mengirimkan pesan', 'success') : setMessage('Gagal mengirimkan pesan', 'danger');
            redirect('beranda');
        }
    }

    public function tentang()
    {
        $data['title'] = 'Tentang';
        $data['active'] = 'tentang';
        $this->template->load('client/templatefront', 'client/home/tentang', $data);
    }

    public function kontak()
    {
        $data['title'] = 'Kontak Kami';
        $data['active'] = 'kontak';
        $this->template->load('client/templatefront', 'client/home/kontak', $data);
    }
}
