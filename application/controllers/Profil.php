<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Profil extends MY_Controller
{
    public $user;

    function __construct()
    {
        parent::__construct();
        if (empty($this->user['idpembeli'])) {
            redirect('beranda');
        }

        $this->load->model('M_users', 'users');
        $this->load->model('M_pembeli', 'pembeli');
        $this->user = $this->users->getBy(['username' => $this->session->userdata('username')])->row_array();
    }

    public function index()
    {
        $data['title'] = "Profil";
        $data['active'] = "profil";
        $data['users'] = $this->user;

        $this->template->load('client/templateprofil', 'client/profil/index', $data);
    }

    public function edit()
    {
        $data['title'] = "Profil";
        $data['active'] = "edit";

        $data['users'] = $this->user;
        $data['pembeli'] = getPembeli()->row_array();
        $data['jk'] = [
            'L' => 'Laki-Laki',
            'P' => 'Perempuan'
        ];
        $this->template->load('client/templateprofil', 'client/profil/edit', $data);

        if ($_POST) {
            $nama = $this->input->post('nama');
            $telepon = $this->input->post('telepon');
            $jk = $this->input->post('jk');
            $idpembeli = $this->input->post('idpembeli');


            $pembeli = [
                'namapembeli' => $nama,
                'telepon' => $telepon,
                'jk' => $jk
            ];

            $this->pembeli->beginTrans();
            $this->pembeli->update($pembeli, $idpembeli);
            $this->users->update(['nama' => $nama], $this->user['iduser']);
            $status = $this->pembeli->statusTrans();
            $this->pembeli->commitTrans($status);

            $status && $status ? setMessage('Berhasil merubah profil', 'success') : setMessage('Gagal mengubah profil', 'danger');
            redirect('profil/edit');
        }
    }

    public function password()
    {
        $data['title'] = "Profil";
        $data['active'] = "password";

        $this->template->load('client/templateprofil', 'client/profil/password', $data);

        if ($_POST) {
            $oldpass = $this->input->post('oldpassword');
            $newpass = $this->input->post('newpassword');
            $newpassconf = $this->input->post('newpasswordconf');

            if (password_verify($oldpass, $this->user['password'])) {
                if ($newpass == $newpassconf) {
                    if ($newpass != $oldpass) {
                        $update = $this->users->update(['password' => password_hash($newpass, PASSWORD_DEFAULT)], $this->user['iduser']);
                        $update ? setMessage('Berhasil merubah password', 'success') : setMessage('Gagal merubah password!', 'danger');
                    } else {
                        setMessage('Password tidak boleh sama dengan yang lama!', 'danger');
                    }
                } else {
                    setMessage('Password tidak cocok!', 'danger');
                }
            } else {
                setMessage('Password salah!', 'danger');
            }

            redirect('profil/password');
        }
    }

    public function alamat($act = null, $id = null)
    {
        $this->load->model('M_alamat', 'alamat');

        $data['title'] = "Profil";
        $data['active'] = "alamat";

        if (empty($act)) {
            $data['alamat'] = $this->alamat->getList(['idpembeli' => $this->user['idpembeli']]);

            $a_kolom = [];
            $a_kolom[] = ['kolom' => 'no', 'label' => 'No'];
            $a_kolom[] = ['kolom' => 'jenisalamat', 'label' => 'Nama Alamat'];
            $a_kolom[] = ['kolom' => 'alamat', 'label' => 'Detail Alamat'];

            $data['a_kolom'] = $a_kolom;
        }


        if (!empty($act)) {
            $a_kolom = [];
            $a_kolom[] = ['kolom' => 'jenisalamat', 'label' => 'Nama Alamat', 'required' => true];
            $a_kolom[] = ['kolom' => 'penerima', 'label' => 'Nama Penerima', 'required' => true];
            $a_kolom[] = ['kolom' => 'alamat', 'label' => 'Alamat', 'required' => true];
            $a_kolom[] = ['kolom' => 'provinsi', 'label' => 'Provinsi', 'required' => true];
            $a_kolom[] = ['kolom' => 'kota', 'label' => 'Kota/Kabupaten', 'required' => true];
            $a_kolom[] = ['kolom' => 'kecamatan', 'label' => 'kecamatan', 'required' => true];
            $a_kolom[] = ['kolom' => 'kodepos', 'label' => 'Kode Pos', 'required' => true];

            if ($_POST['act'] == 'save') {
                $record = [];
                foreach ($a_kolom as $col) {
                    if (!empty($_POST[$col['kolom']]) || $col['required'] == true) {
                        if (empty($_POST[$col['kolom']])) {
                            setMessage($col['label'] . " wajib diisi.", 'danger');
                            if (empty($_POST['key']))
                                redirect('profil/alamat/add');
                            else
                                redirect('profil/alamat/detail/' . $_POST['key']);
                        }
                        $record[$col['kolom']] = $_POST[$col['kolom']];
                    }
                }

                $record['idpembeli'] = $this->user['idpembeli'];

                if (empty($_POST['key'])) {
                    $ok = $this->alamat->insert($record);
                    $msg = "menambah alamat";
                    $ok ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                    if (!$ok)
                        redirect('profil/alamat/add');
                } else {
                    $ok = $this->alamat->update($record, $_POST['key']);
                    $msg = "mengubah alamat";
                    $ok ? setMessage('Berhasil ' . $msg, 'success') : setMessage('Gagal ' . $msg, 'danger');
                    if (!$ok)
                        redirect('profil/alamat/detail/' . $_POST['key']);
                }
                redirect('profil/alamat');
            }

            $data['a_kolom'] = $a_kolom;

            switch ($act) {
                case 'add':
                    $this->template->load('client/templateprofil', 'client/profil/act_alamat', $data);
                    break;
                case 'detail':
                    $data['row'] = $this->alamat->getBy(['idpembeli' => $this->user['idpembeli'], 'idalamat' => $id])->row_array();
                    if (empty($data['row']))
                        redirect('profil/alamat');

                    $this->template->load('client/templateprofil', 'client/profil/act_alamat', $data);
                    break;
                default:
                    redirect('profil/alamat');
                    break;
            }
        } else {
            $this->template->load('client/templateprofil', 'client/profil/alamat', $data);
        }
    }
}
