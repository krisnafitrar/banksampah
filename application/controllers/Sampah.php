<?php

class Sampah extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_users', 'users');
    }

    public function index()
    {
        $data['title'] = 'Sampah | Jenis';
        $data['jenis'] = $this->db->get('jenis_sampah')->result_array();
        $data['active'] = 'jenis';
        $this->template->load('client/templatefront', 'client/sampah/jenis', $data);
    }

    public function golongan()
    {
        $data['title'] = 'Sampah | Golongan';
        $data['golongan'] = $this->db->get('golongan_sampah')->result_array();
        $data['active'] = 'golongan';
        $this->template->load('client/templatefront', 'client/sampah/golongan', $data);
    }
}
