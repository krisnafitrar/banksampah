<?php

class PenarikanTabungan extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Penarikan Tabungan', '#');
        $data['title'] = 'Penarikan Tabungan';
        $data['profile'] = 'Penarikan Tabungan';
        $data['user'] = $this->user;
        $this->template->load('template', 'transaksi/penarikan', $data);

        if ($_POST) {
            $kodepenarikan = $this->input->post('kodepenarikan');
            $penarikan = $this->db->get_where('penarikan', ['kodepenarikan' => $kodepenarikan])->row_array();
            if ($penarikan['status'] == 0) {
                $up = $this->db->update('penarikan', ['status' => 1], ['kodepenarikan' => $kodepenarikan]);
                $nasabah = $this->db->get_where('nasabah', ['idnasabah' => $penarikan['idnasabah']])->row_array();
                $min = $this->db->update('nasabah', ['saldonasabah' => $nasabah['saldonasabah'] - $penarikan['jumlahpenarikan']], ['idnasabah' => $nasabah['idnasabah']]);
                $data = [
                    'koderiwayat' => 'TR-' . time(),
                    'idnasabah' => $nasabah['idnasabah'],
                    'riwayatsaldo' => $penarikan['jumlahpenarikan'],
                    'status' => 'min',
                    'admin' => $this->session->userdata('iduser')
                ];
                $ok = $this->db->insert('riwayat_saldo', $data);
                $up && $min && $ok ? setMessage('Berhasil melakukan penarikan', 'success') : setMessage('Gagal melakukan penarikan', 'danger');
                redirect('nasabah/penarikantabungan');
            } else {
                setMessage('Anda sudah melakukan penarikan', 'danger');
                redirect('nasabah/penarikantabungan');
            }
        }
    }

    public function getPenarikan()
    {
        $keyword = $this->input->post('cari');
        $query = "SELECT * FROM penarikan WHERE lower(kodepenarikan) LIKE '%$keyword%'";
        $penarikan = $this->db->query($query);

        if ($penarikan->num_rows() < 1) {
            echo json_encode(array());
        } else {
            $data = array();
            foreach ($penarikan->result_array() as $val) {
                $data[] = array('id' => $val['kodepenarikan'], 'text' => $val['kodepenarikan'] . ' - ' . toRupiah($val['jumlahpenarikan']));
            }
            echo json_encode($data);
        }
    }
}
