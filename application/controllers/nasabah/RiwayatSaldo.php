<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RiwayatSaldo extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_nasabah', 'nasabah');

        //set default
        $this->title = 'Riwayat Saldo';
        $this->menu = 'riwayatsaldo';
        $this->parent = 'nasabah';
        $this->pager = true;
        $this->is_crud = false;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_nasabah = $this->nasabah->getListCombo();

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'koderiwayat', 'label' => 'Kode'];
        $a_kolom[] = ['kolom' => 'idnasabah', 'label' => 'Nasabah', 'type' => 'S', 'option' => $a_nasabah];
        $a_kolom[] = ['kolom' => 'riwayatsaldo', 'label' => 'Saldo', 'set_currency' => true, 'type' => 'N'];
        $a_kolom[] = ['kolom' => 'status', 'label' => 'Status'];

        $this->a_kolom = $a_kolom;
    }
}
