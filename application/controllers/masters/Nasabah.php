<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Nasabah extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_desa', 'desa');
        $this->load->model('M_rtrw', 'rtrw');
        $this->load->model('M_dusun', 'dusun');

        //set default
        $this->title = 'Data Nasabah';
        $this->menu = 'nasabah';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {

        $a_desa = $this->desa->getListCombo();
        $a_dusun = $this->dusun->getListCombo();
        $a_rtrw = $this->rtrw->getListCombo();

        $a_jk = [
            '1' => 'Laki-Laki',
            '0' => 'Perempuan'
        ];

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'namanasabah', 'label' => 'Nama'];
        $a_kolom[] = ['kolom' => 'jeniskelamin', 'label' => 'JK', 'type' => 'S', 'option' => $a_jk];
        $a_kolom[] = ['kolom' => 'email', 'label' => 'Email', 'type' => 'E', 'is_tampil' => false];
        $a_kolom[] = ['kolom' => 'notelp', 'label' => 'No Telp'];
        $a_kolom[] = ['kolom' => 'iddesa', 'label' => 'Desa', 'type' => 'S', 'option' => $a_desa];
        $a_kolom[] = ['kolom' => 'iddusun', 'label' => 'Dusun', 'type' => 'S', 'option' => $a_dusun];
        $a_kolom[] = ['kolom' => 'idrtrw', 'label' => 'RT/RW', 'type' => 'S', 'option' => $a_rtrw];
        $a_kolom[] = ['kolom' => 'alamatnasabah', 'label' => 'Alamat', 'type' => 'A', 'is_tampil' => false];
        $a_kolom[] = ['kolom' => 'foto', 'label' => 'Foto', 'type' => 'F', 'path' => './assets/img/nasabah/', 'file_type' => 'jpg|png'];
        $a_kolom[] = ['kolom' => 'saldonasabah', 'label' => 'Saldo', 'type' => 'N', 'set_currency' => true];

        $this->a_kolom = $a_kolom;
    }
}
