<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dusun extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_desa', 'desa');

        //set default
        $this->title = 'Data Dusun';
        $this->menu = 'dusun';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {

        $a_desa = $this->desa->getListCombo();

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'namadusun', 'label' => 'Nama Dusun'];
        $a_kolom[] = ['kolom' => 'iddesa', 'label' => 'Desa', 'type' => 'S', 'option' => $a_desa];
        $a_kolom[] = ['kolom' => 'info_tambahan', 'label' => 'Info Tambahan', 'type' => 'A'];

        $this->a_kolom = $a_kolom;
    }
}
