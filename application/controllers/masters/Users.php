<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_desa', 'desa');
        $this->load->model('M_role', 'role');

        //set default
        $this->title = 'Data User';
        $this->menu = 'users';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {

        $a_role = $this->role->getListCombo();
        $a_status = [
            '1' => 'Aktif',
            '0' => 'Non-Aktif'
        ];

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'nama', 'label' => 'Nama'];
        $a_kolom[] = ['kolom' => 'username', 'label' => 'Username'];
        $a_kolom[] = ['kolom' => 'email', 'label' => 'Email', 'type' => 'E'];
        $a_kolom[] = ['kolom' => 'password', 'label' => 'Password', 'type' => 'P', 'is_tampil' => false];
        $a_kolom[] = ['kolom' => 'idrole', 'label' => 'Role', 'type' => 'S', 'option' => $a_role];
        $a_kolom[] = ['kolom' => 'is_aktif', 'label' => 'Status', 'type' => 'S', 'option' => $a_status];

        $this->a_kolom = $a_kolom;
    }
}
