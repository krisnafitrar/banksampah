<?php
defined('BASEPATH') or exit('No direct script access allowed');

class RtRw extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_desa', 'desa');
        $this->load->model('M_dusun', 'dusun');

        //set default
        $this->title = 'Data RT RW';
        $this->menu = 'rtrw';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {

        $a_desa = $this->desa->getListCombo();
        $a_dusun = $this->dusun->getListCombo();

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'rtrw', 'label' => 'RT/RW'];
        $a_kolom[] = ['kolom' => 'iddusun', 'label' => 'Dusun', 'type' => 'S', 'option' => $a_dusun];
        $a_kolom[] = ['kolom' => 'iddesa', 'label' => 'Desa', 'type' => 'S', 'option' => $a_desa];
        $a_kolom[] = ['kolom' => 'info_tambahan', 'label' => 'Info Tambahan', 'type' => 'A', 'is_null' => true];

        $this->a_kolom = $a_kolom;
    }
}
