<?php
defined('BASEPATH') or exit('No direct script access allowed');

class JenisSampah extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_golongansampah', 'golongan');

        //set default
        $this->title = 'Jenis Sampah';
        $this->menu = 'jenissampah';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {

        $a_golongan = $this->golongan->getListCombo();

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'namajenis', 'label' => 'Nama Jenis'];
        $a_kolom[] = ['kolom' => 'idgolongan', 'label' => 'Golongan', 'type' => 'S', 'option' => $a_golongan];
        $a_kolom[] = ['kolom' => 'gambar', 'label' => 'Gambar', 'type' => 'F', 'path' => './assets/img/jenis/', 'file_type' => 'jpg|png'];
        $a_kolom[] = ['kolom' => 'hargasampah', 'label' => 'Harga', 'type' => 'N', 'set_currency' => true];

        $this->a_kolom = $a_kolom;
    }
}
