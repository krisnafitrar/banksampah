<?php

class Tabungan extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_users', 'users');
        $this->load->model('M_nasabah', 'nasabah');
        $this->load->model('M_penarikan', 'penarikan');
    }

    public function index()
    {
        $data['title'] = 'Tabunganku';
        $data['nasabah'] = $this->nasabah->getReff($this->session->userdata('idnasabah'))->row_array();
        $this->template->load('client/templatefront', 'client/tabungan/index', $data);
    }

    public function tariksaldo()
    {
        if ($_POST) {
            $idnasabah = $this->input->post('idnasabah');
            $nominal = $this->input->post('nominal');

            $nasabah = $this->nasabah->getBy(['idnasabah' => $idnasabah])->row_array();

            if ($nasabah['saldonasabah'] >= $nominal) {
                $cekPenarikan = $this->penarikan->getBy(['idnasabah' => $idnasabah])->row_array();
                if ($cekPenarikan['status'] > 0) {
                    $data = [
                        'kodepenarikan' => 'OUT-' . time(),
                        'idnasabah' => $idnasabah,
                        'jumlahpenarikan' => $nominal,
                        'status' => 0
                    ];

                    $ok = $this->penarikan->insert($data);
                    $ok ? setMessage('Berhasil melakukan penarikan', 'success') : setMessage('Gagal melakukan penarikan!', 'danger');
                    redirect('tabungan/penarikan');
                } else {
                    setMessage('Anda sedang melakukan penarikan!', 'danger');
                    redirect('tabungan');
                }
            } else {
                setMessage('Saldo tidak mencukupi!', 'danger');
                redirect('tabungan');
            }
        }
    }

    public function penarikan()
    {
        $data['title'] = 'Penarikan';
        $data['active'] = 'penarikan';
        $data['penarikan'] = $this->penarikan->getBy(['idnasabah' => $this->session->userdata('idnasabah'), 'status' => 0])->row_array();
        $this->template->load('client/templatefront', 'client/tabungan/penarikan', $data);
    }

    public function riwayat()
    {
        $data['title'] = 'Riwayat';
        $data['active'] = 'riwayat';
        $data['riwayatsaldo'] = $this->penarikan->riwayat($this->session->userdata('idnasabah'))->result_array();
        $this->template->load('client/templatefront', 'client/tabungan/penarikan', $data);
    }
}
