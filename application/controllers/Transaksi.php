<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Transaksi extends MY_Controller
{
    public $user;

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //load model
        $this->load->model('M_users', 'users');
        $this->load->model('M_transaksi', 'transaksi');
        $this->load->model('M_nasabah', 'nasabah');
        $this->load->model('M_transaksidetail', 'transaksidetail');
        $this->load->model('M_riwayatsaldo', 'riwayatsaldo');

        //load library
        $this->load->library('cart');

        $this->user = $this->users->getBy(['username' => $this->session->userdata['username']])->row_array();
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Transaksi', '#');
        $data['title'] = 'Transaksi';
        $data['profile'] = 'Transaksi';
        $data['user'] = $this->user;
        $this->template->load('template', 'transaksi/index', $data);

        if (!empty($_POST)) {
            $jenis = $this->input->post('idjenis');
            $berat = $this->input->post('berat');
            $act = $this->input->post('act');
            $key = $this->input->post('key');

            $sampah = $this->db->get_where('jenis_sampah', ['idjenis' => $jenis])->row_array();

            if ($act == "update") {
                $data = [
                    'rowid' => $key,
                    'qty' => $berat,
                ];
                $this->cart->update($data);
            } else {
                $data = [
                    'id' => $jenis,
                    'qty' => $berat,
                    'price' => $sampah['hargasampah'],
                    'name' => $sampah['namajenis'],
                ];
                $this->cart->insert($data);
            }

            redirect('transaksi');
        }
    }

    public function getJenis()
    {
        $keyword = $this->input->post('cari');
        $query = "SELECT * FROM jenis_sampah WHERE lower(namajenis) LIKE '%$keyword%' OR lower(idjenis) LIKE '%$keyword%'";
        $jenis = $this->db->query($query);

        if ($jenis->num_rows() < 1) {
            echo json_encode(array());
        } else {
            $data = array();
            foreach ($jenis->result_array() as $val) {
                $data[] = array('id' => $val['idjenis'], 'text' => $val['idjenis'] . ' - ' . $val['namajenis']);
            }
            echo json_encode($data);
        }
    }

    public function getNasabah()
    {
        $keyword = $this->input->post('cari');
        $query = "SELECT * FROM nasabah WHERE lower(namanasabah) LIKE '%$keyword%' OR lower(idnasabah) LIKE '%$keyword%'";
        $nasabah = $this->db->query($query);

        if ($nasabah->num_rows() < 1) {
            echo json_encode(array());
        } else {
            $data = array();
            foreach ($nasabah->result_array() as $val) {
                $data[] = array('id' => $val['idnasabah'], 'text' => $val['idnasabah'] . ' - ' . $val['namanasabah']);
            }
            echo json_encode($data);
        }
    }

    public function destroyCart()
    {
        $ok = $this->cart->destroy();
        !$ok ? setMessage('Berhasil membersihkan cart', 'success') : setMessage('Gagal membersihkan cart', 'danger');
        redirect('transaksi');
    }

    public function deleteCart($rowid)
    {
        $this->cart->remove($rowid);
        redirect('transaksi');
    }

    public function checkout()
    {
        if (!empty($_POST)) {
            $notrans = $this->input->post('kodetrans');
            $tanggal = $this->input->post('tanggal');
            $nasabah = $this->input->post('nasabah');
            $admin = $this->user['iduser'];
            $cart = $this->cart->contents();
            $total = 0;

            foreach ($cart as $result) {
                $total += $result['subtotal'];
            }

            $a_trans = [
                'kodetransaksi' => $notrans,
                'waktutransaksi' => $tanggal,
                'idnasabah' => $nasabah,
                'total' => $total,
                'admin' => $admin
            ];


            $this->transaksi->beginTrans();
            $this->transaksi->insert($a_trans);

            foreach ($cart as $val) {
                $a_detail = [
                    'kodetransaksi' => $notrans,
                    'idjenis' => $val['id'],
                    'berat' => $val['qty']
                ];

                $this->transaksidetail->insert($a_detail);
            }

            $getNasabah = $this->db->get_where('nasabah', ['idnasabah' => $nasabah])->row_array();

            $a_update = [
                'saldonasabah' => $getNasabah['saldonasabah'] + $total
            ];

            $a_riwayat = [
                'koderiwayat' => $notrans,
                'idnasabah' => $nasabah,
                'riwayatsaldo' => $total,
                'status' => 'plus',
                'admin' => $admin
            ];

            $this->riwayatsaldo->insert($a_riwayat);
            $this->nasabah->update($a_update, $nasabah);

            $ok = $this->transaksi->statusTrans();
            $this->transaksi->commitTrans($ok);


            if ($ok) {
                $this->cart->destroy();
                setMessage('Berhasil menyimpan data transaksi', 'success');
            } else {
                setMessage('Gagal menyimpan data', 'danger');
            }

            redirect('transaksi');
        }
    }

    public function riwayat()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Riwayat Transaksi', '#');
        $data['title'] = 'Riwayat Transaksi';
        $data['profile'] = 'Transaksi';
        $data['user'] = $this->user;
        $data['riwayat'] = $this->transaksi->getTransaksi()->result_array();
        $this->template->load('template', 'transaksi/riwayat', $data);
    }

    public function batalkan($id)
    {
        $trans = $this->db->get_where('transaksi', ['kodetransaksi' => $id])->row_array();
        $nasabah = $this->db->get_where('nasabah', ['idnasabah' => $trans['idnasabah']])->row_array();

        $this->nasabah->beginTrans();
        $this->nasabah->update(['saldonasabah' => $nasabah['saldonasabah'] - $trans['total']], $trans['idnasabah']);
        $this->db->delete('transaksi_detail', ['kodetransaksi' => $id]);
        $this->db->delete('transaksi', ['kodetransaksi' => $id]);
        $this->db->delete('riwayat_saldo', ['koderiwayat' => $id]);

        $ok = $this->nasabah->statusTrans();
        $this->nasabah->commitTrans($ok);

        $ok ? setMessage('Berhasil membatalkan transaksi', 'success') : setMessage('Gagal membatalkan trasaksi', 'danger');
        redirect('transaksi/riwayat');
    }

    public function detail($id)
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', base_url());
        $this->breadcrumb->append_crumb('Riwayat Transaksi', base_url('transaksi/riwayat'));
        $this->breadcrumb->append_crumb('Detail', '#');
        $data['title'] = 'Detail Transaksi';
        $data['profile'] = 'Detail Transaksi';
        $data['user'] = $this->user;
        $data['detail'] = $this->transaksidetail->getReff($id)->result_array();
        $this->template->load('template', 'transaksi/detail', $data);
    }
}
