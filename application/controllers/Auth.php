<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends MY_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        $this->load->model('M_users', 'users');
    }

    public function index()
    {
        if ($this->session->userdata('username')) {
            redirect('home');
        }

        $this->form_validation->set_rules('username', 'Username', 'required|trim');
        $this->form_validation->set_rules('password', 'Password', 'required|trim');
        if ($this->form_validation->run() == false) {
            $data['title'] = "Login Page";
            $this->template->load('template_auth', 'auth/login', $data);
        } else {
            //validasinya success
            $this->login();
        }
    }

    private function login()
    {
        if ($this->session->userdata('username')) {
            redirect('user');
        }

        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $user = $this->users->getByUsername($username)->row_array();
        //jika usernya ada
        if ($user) {
            //cek password
            if (password_verify($password, $user['password'])) {
                $data = [
                    'username' => $user['username'],
                    'idrole' => $user['idrole'],
                    'email' => $user['email'],
                    'idnasabah' => $user['idnasabah']
                ];

                $this->session->set_userdata($data);
                if ($this->session->userdata['idrole'] > 1) {
                    redirect('beranda');
                } else {
                    redirect('home');
                }
            } else {
                //password salah
                setMessage('Password salah !', 'danger');
                redirect('auth');
            }
        } else {
            //usernya tidak ada
            setMessage('User tidak terdaftar', 'danger');
            redirect('auth');
        }
    }

    public function registrasi()
    {
        $data['title'] = "Registrasi";
        $this->template->load('template_auth', 'auth/registration', $data);

        if ($_POST) {
            $nama = $this->input->post('nama');
            $username = $this->input->post('username');
            $email = $this->input->post('email');
            $telp = $this->input->post('telp');
            $password1 = $this->input->post('password1');
            $password2 = $this->input->post('password2');

            if ($password1 == $password2) {
                $data = [
                    'nama' => htmlspecialchars($nama),
                    'email' => htmlspecialchars($email),
                    'password' => password_hash($password1, PASSWORD_DEFAULT),
                    'username' => htmlspecialchars($username),
                    'idpembeli' => $this->pembeli->getAI(),
                    'create_at' => time(),
                    'is_aktif' => 1
                ];

                $pembeli = [
                    'namapembeli' => $nama,
                    'telepon' => $telp,
                ];

                $this->pembeli->beginTrans();
                $this->pembeli->insert($pembeli);
                $this->users->insert($data);

                $check = $this->pembeli->statusTrans();
                $this->pembeli->commitTrans($check);

                $check && $check ? setMessage('Berhasil membuat akun', 'success') : setMessage('Gagal membuat akun!', 'danger');
            } else {
                setMessage('Password tidak cocok!', 'danger');
            }
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('iduser');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('idrole');
        $this->session->unset_userdata('keyword');
        $this->session->unset_userdata('idnasabah');

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Berhasil logout
            </div>');
        redirect('beranda');
    }

    public function blocked()
    {
        $this->load->view('auth/blocked');
    }
}
