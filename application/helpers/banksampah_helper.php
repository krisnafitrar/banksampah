<?php

function is_logged_in()
{
    $ci = get_instance();
    if (!$ci->session->userdata('username')) {
        redirect('auth');
    } else {
    }
}

function getMenu()
{
    return [
        'home' => [
            'key' => 'home',
            'name' => 'Beranda',
            'link' => 'home',
            'icon' => 'home'
        ],
        'master' => [
            'key' => 'master',
            'name' => 'Master',
            'link' => '',
            'icon' => 'tasks',
            'child' => [
                'role' => [
                    'key' => 'role',
                    'name' => 'Role',
                    'link' => 'masters/role'
                ],
                'golongan' => [
                    'key' => 'golongan',
                    'name' => 'Data Golongan Sampah',
                    'link' => 'masters/golongansampah'
                ],
                'jenis' => [
                    'key' => 'jenis',
                    'name' => 'Data Jenis Sampah',
                    'link' => 'masters/jenissampah'
                ],
                'desa' => [
                    'key' => 'desa',
                    'name' => 'Data Desa',
                    'link' => 'masters/desa'
                ],
                'dusun' => [
                    'key' => 'dusun',
                    'name' => 'Data Dusun',
                    'link' => 'masters/dusun'
                ],
                'rtrw' => [
                    'key' => 'rtrw',
                    'name' => 'Data RT RW',
                    'link' => 'masters/rtrw'
                ],
                'users' => [
                    'key' => 'users',
                    'name' => 'Data User',
                    'link' => 'masters/users'
                ],
                'nasabah' => [
                    'key' => 'nasabah',
                    'name' => 'Data Nasabah',
                    'link' => 'masters/nasabah'
                ]
            ]
        ],
        'transaksi' => [
            'key' => 'transaksi',
            'name' => 'Transaksi',
            'link' => '',
            'icon' => 'coins',
            'child' => [
                'transaksi' => [
                    'key' => 'transaksi',
                    'name' => 'Transaksi',
                    'link' => 'transaksi'
                ],
                'detail' => [
                    'key' => 'detail',
                    'name' => 'Riwayat Transaksi',
                    'link' => 'transaksi/riwayat'
                ]
            ]
        ],
        'nasabah' => [
            'key' => 'nasabah',
            'name' => 'Nasabah',
            'link' => '',
            'icon' => 'user-friends',
            'child' => [
                'penarikantabungan' => [
                    'key' => 'penarikantabungan',
                    'name' => 'Penarikan Tabungan',
                    'link' => 'nasabah/penarikantabungan'
                ],
                'riwayattabungan' => [
                    'key' => 'riwayattabungan',
                    'name' => 'Riwayat Tabungan',
                    'link' => 'nasabah/riwayatsaldo'
                ]
            ]
        ],
        'pengaturan' => [
            'key' => 'pengaturan',
            'name' => 'Pengaturan Aplikasi',
            'icon' => 'cogs',
            'link' => 'pengaturan'
        ],
        'logout' => [
            'key' => 'logout',
            'name' => 'Logout',
            'icon' => 'sign-out-alt',
            'link' => 'auth/logout'
        ]
    ];
}

function setMessage($message, $type)
{
    //type adalah tipe dari alert
    //message pesan nya
    $ci = get_instance();
    $message = $ci->session->set_flashdata('message', '<div class="alert alert-' . $type . ' alert-dismissible fade show" role="alert">' . $message . '               
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>');
}

function setPagination($limit = 10, $rows = null, $url, $model = null, $method = null)
{
    //instance ci
    $ci = get_instance();
    //load library
    $ci->load->library('pagination');

    $config['base_url'] = site_url($url);
    $config['total_rows'] = $rows;
    $config['per_page'] = $limit;
    $config['use_page_numbers'] = TRUE;
    $config['page_query_string'] = TRUE;

    //styling
    $config['full_tag_open'] = '<nav aria-label="Page navigation example"><ul class="pagination justify-content-center">';
    $config['full_tag_close'] = '</ul></nav>';

    $config['first_link'] = 'First';
    $config['first_tag_open'] = '<li class="page-item">';
    $config['first_tag_close'] = '</li>';

    $config['last_link'] = 'Last';
    $config['last_tag_open'] = '<li class="page-item">';
    $config['last_tag_close'] = '</li>';

    $config['next_link'] = '&raquo';
    $config['next_tag_open'] = '<li class="page-item">';
    $config['next_tag_close'] = '</li>';

    $config['prev_link'] = '&laquo';
    $config['prev_tag_open'] = '<li class="page-item">';
    $config['prev_tag_close'] = '</li>';

    $config['cur_tag_open'] = '<li class="page-item active"><a class="page-link" href="#">';
    $config['cur_tag_close'] = '</a></li>';

    $config['num_tag_open'] = '<li class="page-item">';
    $config['num_tag_close'] = '</li>';

    $config['attributes'] = array('class' => 'page-link');


    //init
    $ci->pagination->initialize($config);
}

function toRupiah($val)
{
    $x = "Rp " . number_format($val, 0, ',', '.');
    return $x;
}

function settingSIM()
{
    $ci = get_instance();
    $ci->load->model('M_setting', 'setting');

    $setting = $ci->setting->get()->result_array();

    $a_data = array();
    foreach ($setting as $val) {
        $a_data[$val['idpengaturan']] = $val['valuepengaturan'];
    }

    return $a_data;
}

function dd($data)
{
    echo '<pre>';
    print_r($data);
    echo '</pre>';
    die;
}
